package com.example.exercice1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Button createButton;
    private Button viewContactButton;
    private String contact;
    private ArrayList<String> allContacts = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createButton = (Button) findViewById(R.id.create);
        viewContactButton = (Button) findViewById(R.id.viewContact);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText lastname = findViewById(R.id.lastname);
                EditText firstname = findViewById(R.id.firstname);
                EditText phone = findViewById(R.id.phone);

                contact = lastname.getText().toString() + " " + firstname.getText().toString() + " : " + phone.getText().toString();

                allContacts.add(contact);

                Toast.makeText(MainActivity.this, "Contact ajouté", Toast.LENGTH_LONG).show();
            }
        });

        viewContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                intent.putExtra("EXTRA_CONTACTS", allContacts);
                startActivity(intent);
            }
        });
    }
}
