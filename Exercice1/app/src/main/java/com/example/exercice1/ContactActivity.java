package com.example.exercice1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ContactActivity extends MainActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        ArrayAdapter<String> adapter =  new ArrayAdapter<String>(
            this,
            android.R.layout.simple_list_item_1,
            getIntent().getStringArrayListExtra("EXTRA_CONTACTS")
        );

        ListView listView = (ListView) findViewById(R.id.contactList);
        listView.setAdapter(adapter);
    }
}
