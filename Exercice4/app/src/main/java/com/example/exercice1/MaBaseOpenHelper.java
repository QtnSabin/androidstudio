package com.example.exercice1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.ContextThemeWrapper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MaBaseOpenHelper extends SQLiteOpenHelper {
    private static final int BASE_VERSION = 1;

    public static final String BASE_NAME = "ContactsDB";
    public static final String CONTACT_KEY = "id";
    public static final String CONTACT_LASTNAME= "lastname";
    public static final String CONTACT_FIRSTNAME= "firstname";
    public static final String CONTACT_PHONE= "phone";

    public static final String CONTACTS_TABLE = "contacts";
    public static final String REQUETE_CREATION_DB = "create table " + CONTACTS_TABLE + " (" +
            CONTACT_KEY + " INTEGER primary key autoincrement, " +
            CONTACT_LASTNAME + " TEXT, " +
            CONTACT_FIRSTNAME + " TEXT, " +
            CONTACT_PHONE + " TEXT );";
    public static final String CONTACTS_TABLE_DROP = "DROP TABLE IF EXISTS " + CONTACTS_TABLE + ";";

    /**
     * L'instance de la base qui sera manipulée au travers de cette classe
     */
    private SQLiteDatabase maBaseDonnees;

    public MaBaseOpenHelper(Context context) {
        super(context, BASE_NAME, null, BASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(CONTACTS_TABLE_DROP);
        onCreate(db);
    }

    public List<Contact> allContacts() {

        List<Contact> contacts = new LinkedList<Contact>();
        String query = "SELECT * FROM " + CONTACTS_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Contact contact = null;

        if (cursor.moveToFirst()) {
            do {
                contact = new Contact();
                contact.setLastname(cursor.getString(1));
                contact.setFirstname(cursor.getString(2));
                contact.setPhone(cursor.getString(3));
                contacts.add(contact);
            } while (cursor.moveToNext());
        }

        return contacts;
    }

    public void addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CONTACT_LASTNAME, contact.getLastname());
        values.put(CONTACT_FIRSTNAME, contact.getFirstname());
        values.put(CONTACT_PHONE, contact.getPhone());

        db.insert(CONTACTS_TABLE,null, values);
        db.close();
    }
}

