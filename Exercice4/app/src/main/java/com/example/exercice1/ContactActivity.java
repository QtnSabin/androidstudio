package com.example.exercice1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import java.util.List;


public class ContactActivity extends AppCompatActivity {
    private Button viewMainButton;
    private MaBaseOpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        db = new MaBaseOpenHelper(this);
        List<Contact> contacts = db.allContacts();

        if (contacts != null) {
            String[] itemsNames = new String[contacts.size()];

            for (int i = 0; i < contacts.size(); i++) {
                itemsNames[i] = contacts.get(i).toString();
                System.out.println(itemsNames[i]);
            }

            // display like string instances
            ListView list = (ListView) findViewById(R.id.contactList);
            list.setAdapter(new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, itemsNames));
            }

        viewMainButton = (Button) findViewById(R.id.viewMain);

        viewMainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
