package com.example.exercice1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Button createButton;
    private Button viewContactButton;
    private int countViews = 0;
    private MaBaseOpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new MaBaseOpenHelper(this);

        createButton = (Button) findViewById(R.id.create);
        viewContactButton = (Button) findViewById(R.id.viewContact);
        countViews += 1;

        TextView count =(TextView)findViewById(R.id.count);
        count.setText(Integer.toString(countViews));

        Toast.makeText(this, "Création", Toast.LENGTH_SHORT).show();

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText lastname = findViewById(R.id.lastname);
                EditText firstname = findViewById(R.id.firstname);
                EditText phone = findViewById(R.id.phone);

                Contact contact = new Contact(lastname.getText().toString(), firstname.getText().toString(), phone.getText().toString());
                db.addContact(contact);

                Toast.makeText(MainActivity.this, "Contact ajouté", Toast.LENGTH_LONG).show();
            }
        });

        viewContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        countViews += 1;
        TextView count =(TextView)findViewById(R.id.count);
        count.setText(Integer.toString(countViews));

        Toast.makeText(this, "Etat de l'activité sauvegardé", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Toast.makeText(this, "Etat de l'activité restauré", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "L'activité est détruite", Toast.LENGTH_SHORT).show();
    }
}
