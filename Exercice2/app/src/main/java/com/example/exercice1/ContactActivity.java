package com.example.exercice1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;


public class ContactActivity extends AppCompatActivity {
    private Button viewMainButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        ArrayAdapter<String> adapter =  new ArrayAdapter<String>(
            this,
            android.R.layout.simple_list_item_1,
            getIntent().getStringArrayListExtra("EXTRA_CONTACTS")
        );

        ListView listView = (ListView) findViewById(R.id.contactList);
        listView.setAdapter(adapter);

        viewMainButton = (Button) findViewById(R.id.viewMain);

        viewMainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
