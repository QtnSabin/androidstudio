package com.example.exercice1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Button createButton;
    private Button viewContactButton;
    private String contact;
    private int countViews = 0;
    private ArrayList<String> allContacts = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createButton = (Button) findViewById(R.id.create);
        viewContactButton = (Button) findViewById(R.id.viewContact);
        countViews += 1;

        TextView count =(TextView)findViewById(R.id.count);
        count.setText(Integer.toString(countViews));

        Toast.makeText(this, "Création", Toast.LENGTH_SHORT).show();

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText lastname = findViewById(R.id.lastname);
                EditText firstname = findViewById(R.id.firstname);
                EditText phone = findViewById(R.id.phone);

                contact = lastname.getText().toString() + " " + firstname.getText().toString() + " : " + phone.getText().toString() + "/";

                String FILENAME = "contactsFile.txt";

                FileOutputStream fos = null;
                try {
                    fos = openFileOutput(FILENAME, MainActivity.MODE_APPEND);
                    fos.write(contact.getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Toast.makeText(MainActivity.this, "Contact ajouté", Toast.LENGTH_LONG).show();
            }
        });

        viewContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                intent.putExtra("EXTRA_CONTACTS_FILE", readFromFile(MainActivity.this).split("/"));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        countViews += 1;
        TextView count =(TextView)findViewById(R.id.count);
        count.setText(Integer.toString(countViews));

        Toast.makeText(this, "Etat de l'activité sauvegardé", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Toast.makeText(this, "Etat de l'activité restauré", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "L'activité est détruite", Toast.LENGTH_SHORT).show();
    }

    private String readFromFile(Context context) {
        String response = "";
        try {
            InputStream inputStream = context.openFileInput("contactsFile.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append("\n").append(receiveString);
                }
                inputStream.close();
                response = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return response;
    }
}
